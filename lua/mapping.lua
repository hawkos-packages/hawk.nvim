vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
local s = vim.keymap.set

-- Movement
s('n', '<leader>h', '<c-w>h')
s('n', '<leader>j', '<c-w>j')
s('n', '<leader>k', '<c-w>k')
s('n', '<leader>l', '<c-w>l')
s('n', '<leader><left>', '<c-w>h')
s('n', '<leader><down>', '<c-w>j')
s('n', '<leader><up>', '<c-w>k')
s('n', '<leader><right>', '<c-w>l')

-- LSP
s('n', 'K', vim.lsp.buf.hover, { buffer = 0 })
s('n', 'gd', vim.lsp.buf.definition, { buffer = 0 })
-- s('n', '<leader>f', vim.lsp.buf.format, { buffer = 0 })

--
-- Plugins
--

-- Debugging
local dap = require('dap')
s('n', '<F5>', require('dap').continue)
s('n', '<F6>', require('dap').step_into)
s('n', '<F7>', require('dap').step_over)
s('n', '<F8>', require('dap').step_out)
s('n', '<leader>db', require('dap').toggle_breakpoint)
s('n', '<leader>du', require('dapui').toggle)

-- Git
local gs = require('gitsigns')
s('n', '<leader>gb', function() gs.blame_line{full=true} end)

-- Files
local ts = require('telescope.builtin')
s('n', '<Tab>', require('fm-nvim').Xplr)
s('n', '<leader>ff', ts.find_files)
s('n', '<leader>fg', ts.live_grep)
s('n', '<leader>fb', ts.buffers)
s('n', '<leader>fh', ts.help_tags)

-- Neorg
-- I would like to update this to lua commands in the future
-- local org = require('neorg')

-- Metadata
s('n', '<leader>oi', '<cmd>Neorg inject-metadata<CR>')
s('n', '<leader>ou', '<cmd>Neorg update-metadata<CR>')
-- Workspaces
s('n', '<leader>owd', '<cmd>Neorg workspace default<CR>')
s('n', '<leader>owh', '<cmd>Neorg workspace home<CR>')
s('n', '<leader>owj', '<cmd>Neorg workspace journal<CR>')
s('n', '<leader>owt', '<cmd>Neorg workspace tasks<CR>')
-- Journal
s('n', '<leader>ojp', '<cmd>Neorg journal yesterday<CR>')
s('n', '<leader>ojt', '<cmd>Neorg journal today<CR>')
s('n', '<leader>ojn', '<cmd>Neorg journal tomorrow<CR>')
-- Get things done
s('n', '<leader>otv', '<cmd>Neorg gtd views<CR>')
s('n', '<leader>otc', '<cmd>Neorg gtd capture<CR>')
s('n', '<leader>ote', '<cmd>Neorg gtd edit<CR>')

s('n', '<leader>o<Tab>', '<cmd>Neorg toc split<CR>')
s('n', '<leader>og', '<cmd>Neorg keybind all core.norg.esupports.hop.hop-link<CR>')
s('n', '<leader>on', '<cmd>Neorg news all<CR>')
s('n', '<leader>oc', '<cmd>Neorg toggle-concealer<CR>')
