require('dapui').setup()
local io = require('io')
local dap = require('dap')
dap.adapters.lldb = {
  type = 'executable',
  command = '/usr/bin/lldb-vscode', -- adjust as needed, must be absolute path
  name = 'lldb'
}
dap.configurations.rust = {
  {
    name = 'Launch',
    type = 'lldb',
    request = 'launch',
    -- Builds on debugger launch
    initCommands = 'cargo b',
    program = function()
      local cwd = vim.fn.getcwd()
      -- returns index before (b) %w/src and after (a) src
      local b, a = string.find(cwd, "%w*/src")
      local exe
      if (b == nil) then
        -- returns last dir, assumes this as the executable
        exe = string.gsub(cwd, ".*/", "")
      else
        -- returns directory before src, assumes this as the executable
        exe = string.sub(cwd, b, a-4)
        -- returns directory structure preceding src
        cwd = string.sub(cwd, 1, a-4)
      end
      return vim.fn.input('Path to executable: ', cwd .. '/target/debug/' .. exe, 'file')
    end,
    cwd = '${workspaceFolder}',
    stopOnEntry = false,
  }
}
