local s = vim.keymap.set

local types = {
  ['markdown'] = function (opts)
    s('n', '<leader>bd', '<cmd>MarkdownPreviewToggle<CR>', { buffer = 0 })
    -- Below isnt unset after opening *.md file
    vim.wo.wrap = true
  end
}

vim.api.nvim_create_autocmd('BufEnter', {
  group = vim.api.nvim_create_augroup('bufcmd', { clear = true }),
  callback = function (opts)
    if (types[vim.bo[opts.buf].filetype]) then
      types[vim.bo[opts.buf].filetype](opts)
    end
  end,
})
