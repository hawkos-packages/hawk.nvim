local fn = require('fn')

local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

return require('packer').startup({function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- LSP, Treesitter, Snippets
  use 'neovim/nvim-lspconfig'
  use 'folke/lua-dev.nvim'
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }
  use 'p00f/nvim-ts-rainbow'

  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use 'hrsh7th/cmp-nvim-lua'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'L3MON4D3/LuaSnip'
  use 'saadparwaiz1/cmp_luasnip'
  use 'onsails/lspkind.nvim'

  -- Quality of life
  -- Git, Comments, Markdown, Note Taking
  use 'lewis6991/gitsigns.nvim'
  require('gitsigns').setup({
  })
  use {
    'numToStr/Comment.nvim',
    config = function()
      require('Comment').setup()
    end
  }
  use({
      'iamcco/markdown-preview.nvim',
      run = function() vim.fn['mkdp#util#install']() end,
  })

  -- Debugger
  use 'mfussenegger/nvim-dap'
  use { 'rcarriga/nvim-dap-ui', requires = {'mfussenegger/nvim-dap'} }

  -- Colorscheme
  use 'RRethy/nvim-base16'
  local ok, _ = pcall(vim.cmd('colorscheme base16-dracula'))
  use { 'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }
  require('lualine').setup()
  --use 'lukas-reineke/indent-blankline.nvim'
  -- File Management
  use 'is0n/fm-nvim'
  require('fm-nvim').setup {
    on_close = {
      fn.xplr_cwd,
    }
  }
  use { 'nvim-telescope/telescope.nvim',
    tag = '0.1.0',
    requires = { {'nvim-lua/plenary.nvim'} }
  }
  require('telescope').setup()

  use {
    'nvim-neorg/neorg',
    run = ':Neorg sync-parsers',
    config = function()
      require('neorg').setup {
        load = {
          ['core.defaults'] = {},
          ['core.export.markdown'] = {},
          ['core.norg.qol.toc'] = {},
          ['core.norg.manoeuvre'] = {},
          -- ['core.presenter'] = {},
          ['core.norg.completion'] = {
            config = {
              engine = 'nvim-cmp'
            }
          },
          ['core.norg.dirman'] = {
            config = {
              workspaces = {
                home = '~/.neorg/home',
                journal= '~/.neorg/journal',
                tasks = '~/.neorg/tasks'
              },
              autochdir = true,
              index = 'index.norg',
            }
          },
          ['core.norg.concealer'] = {
            config = {
              icon_preset = 'basic',
              content_only = false,
              width = 'content',
            }
          },
          ['core.norg.journal'] = {
            config = {
              workspace = 'journal',
              journal_folder = '',
              strategy = 'nested',
            }
          },
          ['core.gtd.base'] = {
            config = {
              workspace = 'tasks',
            }
          }
        }
      }
    end
  }

  --[[ if ensure_packer then
    require('packer').sync()
  end ]]
end,
config = {
  display = {
    open_fn = function()
      return require('packer.util').float({ border = 'single' })
    end
  }
}})
