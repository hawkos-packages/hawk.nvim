local fn = require('fn')

-- LSP
local lsp = fn.srequire('lspconfig')
local capabilities = fn.srequire('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
lsp.rust_analyzer.setup { capabilities = capabilities }
lsp.sumneko_lua.setup {}
lsp.marksman.setup {}

-- Cmp
local cmp = fn.srequire('cmp')
cmp.setup({
  snippet = {
    expand = function(args)
      fn.srequire('luasnip').lsp_expand(args.body)
    end
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
  }),
  sources = cmp.config.sources({
    { name = 'nvim_lua' },
    { name = 'nvim_lsp' },
    { name = 'path' },
    { name = 'luasnip' },
    { name = 'buffer', keyword_length = 3 }
  }),
  formatting = {
    format = fn.srequire('lspkind').cmp_format {
      with_text = true,
      menu = {
        buffer = '[buf]',
        nvim_lsp = '[LSP]',
        nvim_lua = '[api]',
        path = '[path]',
        luasnip = '[snip]',
        neorg = '[org]',
      }
    }
  },
  experimental = {
    native_menu = false,
    ghost_text = true
  }
})
cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = 'path' },
  }, {
    { name = 'cmdline' },
  }, {
    { name = 'neorg' }
  })
})

-- TreeSitter
local config = fn.srequire('nvim-treesitter.configs')
config.setup {
  ensure_installed = { 'rust', 'lua' },
  sync_install = true,
  auto_install = true,
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = true
  },
  indent = { enable = true }
}

-- Comment
fn.srequire("Comment").setup {
  padding = true,
  mappings = {
    basic = true,
    extra = true,
    extended = true
  }
}

