local source = {}

source.ok = function(inp)
  if not inp then
    return
  end
end

source.srequire = function(pkg)
  local check, res = pcall(require, pkg)
  source.ok(check)
  return res
end

source.xplr_cwd = function()
  local file = vim.fn.readfile("/tmp/fm-nvim")[1]
  if vim.fn.isdirectory(file) == 1 then
    vim.cmd.Xplr(file)
    os.remove("/tmp/fm-nvim")
    --vim.api.nvim_win_set_option(win, "winhl", "Normal")
  end
end

return source;
